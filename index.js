const processer = require('./SMSmessageGenerator').SMSGenerator

var command = "setdigout 1??? 30 ? ? ?"
const expected = "000000000000001F0C0105000000177365746469676F757420313F3F3F203330203F203F203F0100001F94"

try {   
    var output = processer(command)
    var outputPairs = output.match(/.{1,8}/g)
    var expectedPairs = expected.match(/.{1,8}/g)
    // var pairs = []
    // for (var i = 0; i < outputPairs.length && i < expectedPairs.length; i++) {
    //     var arr = [outputPairs[i], expectedPairs[i]]
    //     pairs.push(`${arr[0]}|${arr[1]}`)
    // }
    if (output != expected) {
        console.log(`Fail`)
        console.log(`Output  : ${output}`)
        console.log(`Expected: ${expected}`)
        //console.log(`Pairs: ${pairs}`);
    } else {
        console.log(`Success`)
    }
} catch (e) {
    console.log('Error')
    console.log(e.message);
}