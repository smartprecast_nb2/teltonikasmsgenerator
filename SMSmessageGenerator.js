const CalcCRC16 = require('./CRC16').CalcCRC16

const preamble = "00000000";

/**
 * 
 * @param {Number|String} input 
 * @param {Number} len The desired length of the output
 */
function toHex(input, len) {
    if (typeof input == 'string') {
        var separated = input.match(/.{1}/g)
        separated = separated.map(x => toHex(x.charCodeAt(0), 2))
        return separated.join('').padStart(len, '0')
    }
    if (typeof input != 'number') {
        throw new Error(`Cannot convert type ${typeof input} into hexacimal string.`);
    }
    var hexStr = input.toString(16)
    hexStr = hexStr.toUpperCase()
    hexStr = hexStr.padStart(len, '0')
    return hexStr
}

function processer () {
    var a = ''
    for (var i = 0; i < arguments.length; i++) {
        var item = arguments[i]
        var hex = toHex(item)
        a += '05' + toHex(item.length, 8) + hex
    }
    var commandQuantity = toHex(arguments.length, 2)
    a = '0C' + commandQuantity + a + commandQuantity
    return preamble + toHex(a.length/2, 8) + a + toHex( CalcCRC16(a), 8)
}

exports.SMSGenerator = processer